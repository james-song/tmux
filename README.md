James Song [Tmux][] settings
=====================================

It contains [Tmux] settings used by James song. How to install it is:

    ./install.sh

[Tmux]: https://tmux.github.io/
